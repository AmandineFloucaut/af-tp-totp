package totp.services;

import org.junit.jupiter.api.*;
// DOC -- Mockito - https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html#verification
import org.mockito.Mock;
import org.mockito.Mockito;
import totp.data.UserRepository;
import totp.generators.RandomKeyGenerator;
import totp.users.User;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RegisterServiceTest {

    /*RegisterService registerService;
    RandomKeyGenerator randomKeyGenerator;
    UserRepository userRepository;*/

    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test");
        //registerService = new RegisterService(randomKeyGenerator, userRepository);
    }

    @Test
    void registerTestWithMock(){
        // GIVEN
        // create new mocks for KeyGenerator and UserRepo classes
        RandomKeyGenerator randomKeyGeneratorMock = Mockito.mock(RandomKeyGenerator.class);
        UserRepository userRepositoryMock = Mockito.mock(UserRepository.class);

        // DOC -- when() - https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html#stubbing
        // Indicate to mock what it must return
        Mockito.when(randomKeyGeneratorMock.generateKey()).thenReturn(87654321L);

        RegisterService registerService = new RegisterService(randomKeyGeneratorMock, userRepositoryMock);

        // WHEN
        final Long secretKey = registerService.register("af@g.com");

        // THEN
        assertEquals(87654321L, secretKey);

        // DOC -- Mockito matchers - https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html#argument_matchers
        Mockito.verify(userRepositoryMock).save(Mockito.eq(new User("af@g.com", 87654321L)));
    }

    @AfterEach
    void tearDown() {
        System.out.println("Fin du test");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests");
    }


}
