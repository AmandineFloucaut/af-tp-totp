package totp.generators;

import org.junit.jupiter.api.*;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class RandomKeyGeneratorTest {

    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test");
    }

    @Test
    void generateKeyMethodTest(){
        RandomKeyGenerator rand = new RandomKeyGenerator(new Random(572867526L));
        assertEquals(8678488882942083205L, rand.generateKey());
    }

    @AfterEach
    void tearDown() {
        System.out.println("Fin du test");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests");
    }
}
