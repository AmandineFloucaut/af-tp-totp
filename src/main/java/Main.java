import totp.data.UserRepository;
import totp.generators.RandomKeyGenerator;
import totp.generators.TotpGenerator;
import totp.services.LoginService;
import totp.services.RegisterService;
import totp.users.User;

import java.sql.Timestamp;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        TotpGenerator totpGenerator = new TotpGenerator();

        RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator(new Random());
        UserRepository userRepository = new UserRepository();
        RegisterService registerService = new RegisterService(randomKeyGenerator, userRepository); //
        LoginService loginService = new LoginService(userRepository);

        while (true) {

            final String[] action;
            System.out.println("Tapez l'action à effectuer");
            final String input = new Scanner(System.in).nextLine();
            action = input.split(" ");
            if (action.length == 0) continue;

            switch (action[0]) {
                case "register":
                    Long newUserKey = registerService.register(action[1]);
                    System.out.println("Secret key : " + newUserKey + ". Store it in a safe place.");
                    break;
                case "generate":
                    Long code = totpGenerator.generate(Long.parseLong(action[1]));
                    System.out.println("Le code actuel est " + code);
                    break;
                case "login":
                    final String loginSuccess = loginService.attemptLogin(action[1], Long.parseLong(action[2]));
                    System.out.println(loginSuccess);
                    break;
                case "stop":
                    System.exit(0);
                default:
                    System.out.println("Erreur : action inconnue");
                    break;
            }
        }
    }

 /*   public static void main(String[] args){
        // Demander email au User :
        Scanner scanner = new Scanner(System.in);
        System.out.println("Veuillez indiquer votre email :");
        String userResponse = scanner.next();

        RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator(new Random());
        UserRepository userRepository = new UserRepository();
        RegisterService registerService = new RegisterService(randomKeyGenerator, userRepository);

        long randomKeyNewUser = registerService.register(userResponse);
        System.out.println(randomKeyNewUser);
        System.out.println(userRepository.getUsersList());

        TotpGenerator testTotpGenerator = new TotpGenerator();
        System.out.println(testTotpGenerator.generate(randomKeyNewUser));

    }*/
}
