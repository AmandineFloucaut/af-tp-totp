package totp.services;

import totp.data.UserRepository;
import totp.generators.TotpGenerator;
import totp.users.User;

public class LoginService {

    UserRepository userRepository;

    public LoginService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public String attemptLogin(String email, Long code){
        //UserRepository userRepository = new UserRepository();
        User currentUser = this.userRepository.findBy(email);

        TotpGenerator totpGenerator = new TotpGenerator();
        Long currentCode = totpGenerator.generate(currentUser.getAuthentificationKey());

        String result;

        if(code.equals(currentCode)){
            result = "Vous êtes connecté";
        }
        else {
            result = "Les codes d'authentification ne correspondent pas";
        }

        return result;
    }
}
