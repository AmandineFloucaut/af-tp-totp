package totp.services;

import totp.data.UserRepository;
import totp.generators.RandomKeyGenerator;
import totp.users.User;

import java.util.Random;

public class RegisterService {

    private final RandomKeyGenerator randomKeyGenerator;
    private final UserRepository userRepository;

    public RegisterService(RandomKeyGenerator randomKeyGenerator, UserRepository userRepository){
        this.randomKeyGenerator = randomKeyGenerator;
        this.userRepository = userRepository;
    }

    public Long register(String email){
        // TODO
        //RandomKeyGenerator generatorKey = new RandomKeyGenerator(new Random());
        Long newKey = this.randomKeyGenerator.generateKey();
        User currentUser = new User(email, newKey);

        this.userRepository.save(currentUser);
        return currentUser.getAuthentificationKey();
    }
}
