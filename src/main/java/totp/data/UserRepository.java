package totp.data;

import totp.users.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private HashMap<String,User> usersList = new HashMap<>();

    public void save(User currentUser) {
        usersList.put(currentUser.getEmail(), currentUser);
    }

    public User findBy(String email){
        return this.usersList.get(email);
    }

    public HashMap<String,User> getUsersList() {
        return this.usersList;
    }

    @Override
    public String toString() {
        String users = "";
        for (Map.Entry entry : usersList.entrySet()) {
            users += "Clé : " + entry.getKey()
                    + " Utilisateur : " + entry.getValue();
        }
        return users;
    }
}
