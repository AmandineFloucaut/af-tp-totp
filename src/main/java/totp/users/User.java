package totp.users;

import java.util.Objects;

public class User {

    private String email;
    private Long authentificationKey;

    public User(String email, Long authentificationKey){
        this.email = email;
        this.authentificationKey = authentificationKey;
    }

    public String getEmail() {
        return this.email;
    }

    public Long getAuthentificationKey() {
        return this.authentificationKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(email, user.email) && Objects.equals(authentificationKey, user.authentificationKey);

       /* Solution Benoit :
       if(!(o instanceof User other)){
            return false;
        } else {
            return other.email.equals(this.email) && other.authentificationKey.equals(this.authentificationKey);
        }*/
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, authentificationKey);
    }
}
