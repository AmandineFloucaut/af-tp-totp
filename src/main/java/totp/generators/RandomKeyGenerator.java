package totp.generators;

import java.util.Random;

public class RandomKeyGenerator {
    private Random rand;

    public RandomKeyGenerator(Random random){
        this.rand = random;
    }

    public Long generateKey(){
        return Math.abs(this.rand.nextLong());
    }
}
