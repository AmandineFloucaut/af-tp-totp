package totp.generators;

import java.time.Instant;

public class TotpGenerator {

    public Long generate(Long randomKey){
        // Determination of current period
        // DOC -- Instant class - https://docs.oracle.com/javase/8/docs/api/java/time/Instant.html
        Long timestamp = Instant.now().getEpochSecond()/300;

        // Combinate random key and current period (hash)
        randomKey += timestamp;

        // Return the currentCode common to User and Server
        return randomKey % 1000000;
    }
}
